import { readFileSync, writeFileSync } from "fs"
import fetch from "node-fetch"
import { getEpochMinute, wait } from "./helpers"

const REQUEST_INTERVAL = 1000 * 30
const RATELIMIT_COOLDOWN = 1000 * 60 * 60 * 2

export const stats: {
  serverStart: number,
  lastRequestAt: number,
  nextRequestAt: number,
  requests: Array<{ time: number, minute: number, status: number }>,
  ratelimits: Array<{ time: number, minute: number, requests: number }>,
  requestsSinceLastRatelimit: number,
  cookie: string
} = {
  serverStart: Date.now(),
  lastRequestAt: 0,
  nextRequestAt: Date.now(),
  requests: [],
  ratelimits: [],
  requestsSinceLastRatelimit: 0,
  cookie: ''
}

const queue: Array<{ nonce: number, fn: () => Promise<{ statusCode: number, body?: any }> }> = []
const results: { [nonce: number]: any } = {}

async function getCookie() {
  const headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-GB,en;q=0.7",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "Host": "steamcommunity.com",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "none",
    "Sec-Fetch-User": "?1",
    "Sec-GPC": "1",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
  }

  const res = await fetch(`https://www.steamcommunity.com`, { method: 'get', headers })

  const cookieHeaders = res?.headers.raw()['set-cookie']

  if (!cookieHeaders) {
    throw new Error('Cannot find cookie')
  }

  const sessionidCookie = cookieHeaders.find(o => o.startsWith('sessionid'))
  const steamCountryCookie = cookieHeaders.find(o => o.startsWith('steamCountry'))

  const sessionid = sessionidCookie?.match(/sessionid=[a-zA-Z0-9]+;/)?.[0]
  const steamCountry = steamCountryCookie?.match(/steamCountry=[^;]+;/)?.[0]

  return `${sessionid} ${steamCountry} timezoneOffset=0,0`
}

function getCookiesFromHeader(setCookie: string[]) {
  const sessionidCookie = setCookie.find(o => o.startsWith('sessionid'))
  const steamCountryCookie = setCookie.find(o => o.startsWith('steamCountry'))

  const sessionid = sessionidCookie?.match(/sessionid=[a-zA-Z0-9]+;/)?.[0]
  const steamCountry = steamCountryCookie?.match(/steamCountry=[^;]+;/)?.[0]

  return `${sessionid} ${steamCountry} timezoneOffset=0,0`
}

async function getInitialInventoryPage(steamid64: string) {
  const headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-GB,en;q=0.8",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "Host": "steamcommunity.com",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "none",
    "Sec-Fetch-User": "?1",
    "Sec-GPC": "1",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
  }

  const res = await fetch(`https://steamcommunity.com/profiles/${steamid64}/inventory`, { headers })

  if (res.status !== 200) {
    throw new Error(`Initial inventory page request was not successful (expected 200 but got ${res.status})`)
  }

  const cookieHeaders = res?.headers.raw()['set-cookie']

  if (!cookieHeaders) {
    throw new Error('Cannot find cookie')
  }

  return getCookiesFromHeader(cookieHeaders)
}

export async function getInventory(steamid: string) {
  const nonce = Date.now() + Math.round(Math.random() * 1000)

  const job = async () => {
    while (stats.nextRequestAt > Date.now()) {
      await wait(1000)
    }

    stats.nextRequestAt = Date.now() + REQUEST_INTERVAL
    stats.requestsSinceLastRatelimit++

    //const cookieStr = await getInitialInventoryPage(steamid)
    const url = `https://steamcommunity.com/inventory/${steamid}/440/2?l=english&count=75`

    const headers = {
      "Accept": "*/*",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "en-GB,en;q=0.8",
      "Connection": "keep-alive",
      "Cookie": stats.cookie || "",
      "Host": "steamcommunity.com",
      "Referer": `https://steamcommunity.com/profiles/${steamid}/inventory`,
      "Sec-Fetch-Dest": "empty",
      "Sec-Fetch-Mode": "cors",
      "Sec-Fetch-Site": "same-origin",
      "Sec-GPC": "1",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36",
      "X-Requested-With": "XMLHttpRequest"
    }

    const res = await fetch(url, { headers })
    const text = await res.text()

    stats.requests.push({ time: Date.now(), minute: getEpochMinute(), status: res.status })
    stats.lastRequestAt = Date.now()

    if (res.status === 429) {
      stats.nextRequestAt = Date.now() + RATELIMIT_COOLDOWN
      stats.ratelimits.push({ time: Date.now(), minute: getEpochMinute(), requests: stats.requestsSinceLastRatelimit })
      stats.requestsSinceLastRatelimit = 0
      console.warn(`RATELIMITED`)
      return { statusCode: 429 }
    }

    try {
      return { statusCode: res.status, body: JSON.parse(text) }
    } catch (e: any) {
      console.error(`Failed to deserialize body because ${e.message}`)
      return { statusCode: 500 }
    }
  }

  queue.push({ nonce, fn: job })

  while (!results[nonce]) {
    await wait(100)
  }

  const result = results[nonce]

  delete results[nonce]
  return result
}

async function processQueue() {
  while (true) {
    if (!queue.length) {
      await wait(100)
      continue
    }

    const task = queue.splice(0, 1)[0]
    try {
      results[task.nonce] = await task.fn()
    } catch (e: any) {
      results[task.nonce] = { statusCode: 500 }
      console.error(`Failed job because ${e.message}`)
    }
  }
}

export async function start() {
  if (!restoreStats()) {
    stats.cookie = await getCookie()
  }
  processQueue()

  setInterval(() => {
    writeFileSync('stats.json', JSON.stringify({ time: new Date().toString(), stats }, null, 2))
  }, 1000 * 20)
}

function restoreStats() {
  try {
    const file = readFileSync('stats.json', { encoding: 'utf-8' })
    const json = JSON.parse(file).stats
    stats.cookie = json.cookie
    stats.lastRequestAt = json.lastRequestAt
    stats.nextRequestAt = json.nextRequestAt
    stats.ratelimits = json.ratelimits
    stats.requests = json.requests
    stats.requestsSinceLastRatelimit = json.requestsSinceLastRatelimit
    stats.serverStart = json.serverStart
    // // TEMP
    // const nextAt = json.requests[json.requests.length - 1].time + RATELIMIT_COOLDOWN
    // stats.nextRequestAt = nextAt
    return true
  } catch (e: any) {
    console.error(`Failed restoring old stats.json because ${e.message}`)
    return false
  }
}