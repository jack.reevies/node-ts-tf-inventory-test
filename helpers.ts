export function wait(ms: number) { return new Promise(resolve => setTimeout(resolve, ms)) }

export function getEpochMinute(time?: number) {
  if (!time) {
    time = Date.now()
  }
  return Math.round((time / 1000) / 60)
}