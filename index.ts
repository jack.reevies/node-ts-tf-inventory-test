import 'source-map-support/register'
import steamids from "./steamids.json"
import polka from 'polka'
import { getInventory, start as initSteamRequest, stats } from "./steamRequest"

async function start() {
  server()
  initSteamRequest()

  for (const steamid of steamids) {
    try { await getInventory(steamid) }
    catch (e) { }
  }
}

function server() {
  polka()
    .get('/', (req, res) => {
      res.statusCode = 200
      res.setHeader('content-type', 'application/json')
      res.end(JSON.stringify(stats))
    })
    .listen(process.env.PORT || 8000)
}

start()